package Controlers;

import java.util.ArrayList;

import Models.Usuario;

public class ControlerUsuario {
	
	ArrayList<Usuario> listaMedico;
	ArrayList<Usuario> listaPaciente;
	public ControlerUsuario(){
		listaMedico = new ArrayList<Usuario>();
		listaPaciente = new ArrayList<Usuario>();
	}
	public void adicionarUmMedico(Usuario umMedico){
		listaMedico.add(umMedico);
	}
	public void adicionarUmPaciente(Usuario umPaciente){
		listaPaciente.add(umPaciente);
	}
	public Usuario buscarMedico(String umMedico){
		for (Usuario buscaMedico: listaMedico){
			if(buscaMedico.getNome().equalsIgnoreCase(umMedico)){
				return buscaMedico;
			}
		}
		return null;
	}
	public Usuario buscarPaciente(String umPaciente){
		for (Usuario buscaPaciente: listaPaciente){
			if(buscaPaciente.getNome().equalsIgnoreCase(umPaciente)){
				return buscaPaciente;
			}
		}
		return null;
	}
	public void removerMedico(Usuario umMedico){
		listaMedico.remove(umMedico);
	}
	public void removerPaciente(Usuario umPaciente){
		listaPaciente.remove(umPaciente);
	}
}
